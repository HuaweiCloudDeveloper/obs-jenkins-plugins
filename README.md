# Jenkins华为云插件

## 工作原理
![上传](image/jenkins.png)

## 插件使用
#### 下载源码&编译

```bash
git clone https://gitee.com/HuaweiCloudDeveloper/huaweicloud-jenkins-plugins.git
cd huaweicloud-jenkins-plugins
mvn package -DskipTests
```
#### 上传插件

>系统管理 --> 插件管理 --> 高级 --> Deploy Plugin --> 选择文件 --> 上传target下的hpi文件 --> 重启Jenkins

## 1、OBS上传文件功能

#### 传统构建后上传功能

##### 增加构建后步骤

##### 修改项目，增加`构建后操作`，选择`华为云OBS上传`

![上传](image/step1.png)

##### 填写华为云OBS配置信息

![参数](image/step2.png)

>本地路径为相对于workspace的路径，例如填写为/abc，则本地路径为${WORKSPACE}/abc
>
>本地路径可以设置为文件或目录。如果设置为文件则上传单个文件，设置为目录上传整个目录

#### Pipeline流水线上传

##### 参数说明

| 参数名称           | 参数说明                                                     |
| ------------------ | ------------------------------------------------------------ |
| ak                 | Access Key                                                   |
| sk                 | Secret Key                                                   |
| credentialsId      | 全局密钥，密钥类型：Secret text，格式：credentialsId:'yourAk,yourSk' |
| bucket             | Bucket                                                       |
| endpoint           | Endpoint                                                     |
| workingDir         | 当前项目的工作目录，不为空时，将从${workspace}/workingDir下查找需要上传的文件 |
| file               | 文件名称，用于指定要上传的文件全名，不能与includePathPattern一起使用 |
| includePathPattern | 文件正则匹配路径，用于匹配要上传的文件，不能与file一起使用   |
| excludePathPattern | 排除文件名正则匹配路径                                       |
| path               | obs远端目录，上传的文件将按照工作空间下的目录结构放到此目录下,不填写默认为根目录 |
| contentType        | [文件类型](https://support.huaweicloud.com/ugobs-obs/obs_41_0025.html#obs_41_0025__section1973224795419) |
| kmsId              | [SSE-KMS key_id](https://support.huaweicloud.com/api-obs/obs_04_0106.html)，加密用 |
| acl                | [文件权限策略](https://support.huaweicloud.com/sdk-go-devg-obs/obs_33_0203.html) |

##### 片段生成器生成

ak/sk示例1：OBSPipelineUpload(ak:"...",sk:"...",endpoint:"obs.cn-south-1.myhuaweicloud.com",bucket:"obs-jenkins", includePathPattern:'***/**', path:'path/to/targetFolder/', workingDir:'/', contentType:'application/x-font-ttf', contentDisposition:'attachment',kmsId:'...')

密钥示例2：OBSPipelineUpload acl: 'Private', ak: '', bucket: 'obs-jenkins', contentType: 'application/x-font-ttf', credentialsId: 'global-obs-credentialsId', endpoint: 'obs.cn-south-1.myhuaweicloud.com', file: '', includePathPattern: '**/*', kmsId: '.....', path: 'path/to/targetFolder/', sk: '', workingDir: 'target'

![上传](image/pipeline.jpg)


## 2、OBS文件下载功能

#### Pipeline流水线下载文件

| 参数名称         | 参数说明                           |
|--------------|--------------------------------|
| ak           | Access Key                     |
| sk           | Secret Key                     |
| endpoint     | 终端节点                           |
| bucket       | 需要下载对象所在桶名                     |
| remotePrefix | 需要下载对象对象的前缀名                   |
| localFolder  | 需要下载到的本地路径(在jenkins的安装路径下存放文件) |

##### 片段生成器生成
示例：OBSPipelineDownload( ak: '', bucket: '', endpoint: '', localFolder: '', remotePrefix: '', sk: '')

![上传](image/obsDownload.jpg)

## 3、CodeArts Check代码检查功能

#### Pipeline流水线触发

##### 参数说明

| 参数名称       | 参数说明                    |
|------------|-------------------------|
| ak         | Access Key              |
| sk         | Secret Key              |
| region     | Region:所在区域，如cn-north-4 |
| project_id | codeCheck中的项目id         |
| task_id    | codeCheck中的任务id         |
| deleteTask | 当门禁质量通过时，是否删除任务，填true删除 |
| topicUrn   | SMN服务中的主题URN，非必填        |

##### 片段生成器生成

示例：CodeCheckPipeline(ak:'',sk'',region:'',project_id:'',task_id:'', deleteTask:'', topicUrn:'')

![上传](image/checkpipeline.jpg)

## 4、VSS漏洞扫描功能

#### Pipeline流水线触发

##### 参数说明

| 参数名称           | 参数说明                                           |
|----------------|------------------------------------------------|
| ak             | Access Key                                     |
| sk             | Secret Key                                     |
| region         | Region:所在区域，如cn-north-4                        |
| task_name      | 扫描任务名称，自定义即可                                   |
| url            | vss扫描的url                                      |
| topicUrn       | SMN服务中的主题URN，非必填                               |
| task_type      | 扫描任务类型，非必填                                     |
| timer          | 普通任务的定时启动时间，非必填                                |
| trigger_time   | 监测任务的定时触发时间，非必填                                |
| task_period    | 监测任务的定时触发周期，非必填                                |
| scan_mode      | 扫描模式:fast - 快速扫描 normal - 标准扫描 deep - 深度扫描，非必填 |
| port_scan      | 是否进行端口扫描，非必填                                   |
| weak_pwd_scan  | 是否进行弱密码扫描，非必填                                  |
| cve_check      | 是否进行CVE漏洞扫描，非必填                                |
| picture_check  | 是否进行网站内容合规文字检测，非必填                             |
| malicious_code | 是否进行网站挂马检测，非必填                                 |
| malicious_link | 是否进行链接健康检测(死链、暗链、恶意外链)，非必填                     |

##### 片段生成器生成

示例：VssPipeline(ak:'',sk:'',region:'cn-north-4',task_name:'test',url:'')

![上传](image/vsspipeline.jpg)


## 5、CodeArts流水线

#### Pipeline触发CodeArts流水线

##### 参数说明

| 参数名称        | 参数说明                    |
|-------------|-------------------------|
| ak          | Access Key              |
| sk          | Secret Key              |
| region      | Region:所在区域，如cn-north-4 |
| project_id  | codeArts项目id            |
| pipeline_id | codeArts流水线id           |

##### 片段生成器生成

示例：CodeArtsPipeline(ak:'',sk:'',region:'cn-north-4',project_id:'',pipeline_id:'')

![上传](image/codeArtsPipeline.jpg)


## 6、DEW服务查询凭据的版本与凭据值

### 查询指定凭据版本的信息和版本中的明文凭据值

#### Pipeline查询指定凭据

##### 参数说明

| 参数名称       | 参数说明                    |
|------------|-------------------------|
| accessKey  | Access Key              |
| secretKey  | Secret Key              |
| region     | Region:所在区域，如cn-north-4 |
| secretName | 凭证名称                    |
| versionId  | 凭证版本                    |

##### 片段生成器生成

示例：getContracts(accessKey: '', region: '', secretKey: '', secretName: '', versionId: '')

![上传](image/DEWCredentials.jpg)




## 构建

1.修改${USER}/.m2/settings.xml中的maven配置文件

在`mirrors`节点中增加
```xml
<mirror>
  <id>repo.jenkins-ci.org</id>
  <url>https://repo.jenkins-ci.org/public/</url>
  <mirrorOf>m.g.o-public</mirrorOf>
</mirror>
```
在`pluginGroups`节点中增加
```xml
<pluginGroup>org.jenkins-ci.tools</pluginGroup>
```
在`profiles`节点中增加
```xml
<profile>
  <id>jenkins</id>
  <activation>
    <activeByDefault>true</activeByDefault>
  </activation>
  <repositories>
    <repository>
      <id>repo.jenkins-ci.org</id>
      <url>https://repo.jenkins-ci.org/public/</url>
    </repository>
  </repositories>
  <pluginRepositories>
    <pluginRepository>
      <id>repo.jenkins-ci.org</id>
      <url>https://repo.jenkins-ci.org/public/</url>
    </pluginRepository>
  </pluginRepositories>
</profile>
```

2.打包
```bash
mvn clean package -DskipTests
```

3.运行
```bash
mvn clean hpi:run
```

## jenkins-plugins详情
关于更多huaweicloud-jenkins-plugins的信息和案例可前往华为云官网查看
https://developer.huaweicloud.com/develop/resource/opensource/jenkins.html

## 在华为云应用商店快速应用
已放入华为云应用商店，您只需开通一个华为云账户，即可一键安装部署好，可直接访问使用。
https://marketplace.huaweicloud.com/contents/5de07bbe-e025-44e4-b065-808c302d9a91#productid=OFFI820561133680414720
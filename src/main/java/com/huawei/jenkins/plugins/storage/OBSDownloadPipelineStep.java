/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.jenkins.plugins.storage;

import hudson.EnvVars;
import hudson.Extension;
import hudson.FilePath;
import hudson.model.TaskListener;
import org.apache.commons.lang.StringUtils;
import org.jenkinsci.plugins.plaincredentials.impl.StringCredentialsImpl;
import org.jenkinsci.plugins.workflow.steps.*;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.DataBoundSetter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * pipeline语法支持obs文件上传
 */
public class OBSDownloadPipelineStep extends Step {
    private String ak;

    private String sk;

    private String bucket;

    private String endpoint;

    private String remotePrefix;

    private String localFolder;


    public String getAk() {
        return ak;
    }

    @DataBoundSetter
    public void setAk(String ak) {
        this.ak = ak;
    }

    public String getSk() {
        return sk;
    }

    @DataBoundSetter
    public void setSk(String sk) {
        this.sk = sk;
    }

    public String getBucket() {
        return bucket;
    }

    @DataBoundSetter
    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getEndpoint() {
        return endpoint;
    }

    @DataBoundSetter
    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getRemotePrefix() {
        return remotePrefix;
    }

    @DataBoundSetter
    public void setRemotePrefix(String remotePrefix) {
        this.remotePrefix = remotePrefix;
    }

    public String getLocalFolder() {
        return localFolder;
    }

    @DataBoundSetter
    public void setLocalFolder(String localFolder) {
        this.localFolder = localFolder;
    }

    @DataBoundConstructor
    public OBSDownloadPipelineStep(String bucket, String endpoint) {
        this.bucket = bucket;
        this.endpoint = endpoint;
    }

    @Override
    public StepExecution start(StepContext context) throws Exception {
        return new Execution(this, context);
    }

    @Extension
    public static class DescriptorImpl extends StepDescriptor {

        @Override
        public Set<? extends Class<?>> getRequiredContext() {
            return requires(TaskListener.class, EnvVars.class, FilePath.class);
        }

        @Override
        public String getFunctionName() {
            return "OBSPipelineDownload";
        }

        @Override
        public String getDisplayName() {
            return "华为云OBS文件下载";
        }
    }

    public static <T extends Class<?>> Set<T> requires(T... classes) {
        return new HashSet<>(Arrays.asList(classes));
    }

    public static class Execution extends SynchronousNonBlockingStepExecution<String> {

        protected static final long serialVersionUID = 1L;

        protected final transient OBSDownloadPipelineStep step;

        public Execution(OBSDownloadPipelineStep step, StepContext context) {
            super(context);
            this.step = step;

        }

        @Override
        public String run() throws Exception {
            CustomInput customInput = new CustomInput();
            customInput.setAk(step.getAk());
            customInput.setSk(step.getSk());
            customInput.setBucket(step.getBucket());
            customInput.setEndpoint(step.getEndpoint());
            customInput.setRemotePrefix(step.getRemotePrefix());
            customInput.setLocalFolder(step.getLocalFolder());
            TaskListener listener = Execution.this.getContext().get(TaskListener.class);
            return ObsService.download(listener, customInput);
        }
    }
}

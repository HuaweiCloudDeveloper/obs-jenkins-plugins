package com.huawei.jenkins.plugins.storage;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;

/**
 * 用户自定义输出参数
 */
public class CustomInput implements Serializable {
    private static final long serialVersionUID = 1L;

    private String ak;

    private String sk;

    private String bucket;

    private String endpoint;

    // 本地文件路径
    private String file;

    // 桶上文件路径
    private String path;

    // kms加密密钥id
    private String kmsId;

    // 包含文件正则路径
    private String includePathPattern;

    // 排除文件正则路径
    private String excludePathPattern;

    // 本地文件根目录
    private String workingDir;

    // 用户自定义标签，暂未使用
    private String[] metadatas;

    // 访问权限设置
    private String acl;

    // 指定对象被下载时的网页的缓存格式
    private String cacheControl;

    // 缓存过期时间
    private String expires;

    // 指定对象被下载时的内容编码格式
    private String contentEncoding;

    // 设置对象的文件类型
    private String contentType;

    // 提供一个默认的文件名赋值给该对象
    private String contentDisposition;

    // 重定向
    private String redirectLocation;

    // 下载对象的前缀名
    private String remotePrefix;

    //下载到的本地路径
    private String localFolder;

    public String getAk() {
        return ak;
    }

    public void setAk(String ak) {
        this.ak = ak;
    }

    public String getSk() {
        return sk;
    }

    public void setSk(String sk) {
        this.sk = sk;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path){
        this.path = path;
    }

    public String getKmsId() {
        return kmsId;
    }

    public void setKmsId(String kmsId) {
        this.kmsId = kmsId;
    }

    public String getIncludePathPattern() {
        return StringUtils.isBlank(includePathPattern) ? null : includePathPattern;
    }

    public void setIncludePathPattern(String includePathPattern) {
        this.includePathPattern = includePathPattern;
    }

    public String getExcludePathPattern() {
        return StringUtils.isBlank(excludePathPattern) ? null : excludePathPattern;
    }

    public void setExcludePathPattern(String excludePathPattern) {
        this.excludePathPattern = excludePathPattern;
    }

    public String getWorkingDir() {
        return workingDir;
    }

    public void setWorkingDir(String workingDir) {
        this.workingDir = workingDir;
    }

    public String[] getMetadatas() {
        return metadatas;
    }

    public void setMetadatas(String[] metadatas) {
        this.metadatas = metadatas;
    }

    public String getAcl() {
        return acl;
    }

    public void setAcl(String acl) {
        this.acl = acl;
    }

    public String getCacheControl() {
        return cacheControl;
    }

    public void setCacheControl(String cacheControl) {
        this.cacheControl = cacheControl;
    }

    public String getExpires() {
        return expires;
    }

    public void setExpires(String expires) {
        this.expires = expires;
    }

    public String getContentEncoding() {
        return contentEncoding;
    }

    public void setContentEncoding(String contentEncoding) {
        this.contentEncoding = contentEncoding;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentDisposition() {
        return contentDisposition;
    }

    public void setContentDisposition(String contentDisposition) {
        this.contentDisposition = contentDisposition;
    }

    public String getRedirectLocation() {
        return redirectLocation;
    }

    public void setRedirectLocation(String redirectLocation) {
        this.redirectLocation = redirectLocation;
    }

    public String getRemotePrefix() {
        return remotePrefix;
    }

    public void setRemotePrefix(String remotePrefix) {
        this.remotePrefix = remotePrefix;
    }

    public String getLocalFolder() {
        return localFolder;
    }

    public void setLocalFolder(String localFolder) {
        this.localFolder = localFolder;
    }
}

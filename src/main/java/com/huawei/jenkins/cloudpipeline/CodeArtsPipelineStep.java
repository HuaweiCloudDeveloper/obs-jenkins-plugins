/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.jenkins.cloudpipeline;

import hudson.EnvVars;
import hudson.Extension;
import hudson.FilePath;
import hudson.model.TaskListener;
import org.jenkinsci.plugins.workflow.steps.*;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.DataBoundSetter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * pipeline语法支持obs文件上传
 */
public class CodeArtsPipelineStep extends Step {

    private String ak;

    private String sk;

    private String region;

    private String project_id;

    private String pipeline_id;

    private String topicUrn;

    public String getAk() {
        return ak;
    }

    @DataBoundSetter
    public void setAk(String ak) {
        this.ak = ak;
    }

    public String getSk() {
        return sk;
    }

    @DataBoundSetter
    public void setSk(String sk) {
        this.sk = sk;
    }

    public String getRegion() {
        return region;
    }

    @DataBoundSetter
    public void setRegion(String region) {
        this.region = region;
    }

    public String getProject_id() {
        return project_id;
    }

    @DataBoundSetter
    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getPipeline_id() {
        return pipeline_id;
    }

    @DataBoundSetter
    public void setPipeline_id(String pipeline_id) {
        this.pipeline_id = pipeline_id;
    }

    public String getTopicUrn() {
        return topicUrn;
    }

    @DataBoundSetter
    public void setTopicUrn(String topicUrn) {
        this.topicUrn = topicUrn;
    }

    @DataBoundConstructor
    public CodeArtsPipelineStep(String region) {
        this.region = region;
    }

    @Override
    public StepExecution start(StepContext context) throws Exception {
        return new Execution(this, context);
    }

    @Extension
    public static class DescriptorImpl extends StepDescriptor {

        @Override
        public Set<? extends Class<?>> getRequiredContext() {
            return requires(TaskListener.class, EnvVars.class, FilePath.class);
        }

        @Override
        public String getFunctionName() {
            return "CodeArtsPipeline";
        }

        @Override
        public String getDisplayName() {
            return "华为云CodeArts流水线";
        }
    }

    public static <T extends Class<?>> Set<T> requires(T... classes) {
        return new HashSet<>(Arrays.asList(classes));
    }

    public static class Execution extends SynchronousNonBlockingStepExecution<String> {

        protected static final long serialVersionUID = 1L;

        protected final transient CodeArtsPipelineStep step;

        public Execution(CodeArtsPipelineStep step, StepContext context) {
            super(context);
            this.step = step;

        }

        @Override
        public String run() throws Exception {
            CustomInput customInput = new CustomInput();
            customInput.setAk(step.getAk());
            customInput.setSk(step.getSk());
            customInput.setRegion(step.getRegion());
            customInput.setProject_id(step.getProject_id());
            customInput.setPipeline_id(step.getPipeline_id());
            customInput.setTopicUrn(step.getTopicUrn());
            TaskListener listener = Execution.this.getContext().get(TaskListener.class);
            return CodeArtsPipelineService.runPipeline(listener, customInput);
        }
    }
}

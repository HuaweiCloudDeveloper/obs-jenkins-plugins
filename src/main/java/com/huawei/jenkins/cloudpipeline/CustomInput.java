package com.huawei.jenkins.cloudpipeline;

import java.io.Serializable;

/**
 * 用户自定义输出参数
 */
public class CustomInput implements Serializable {
    private static final long serialVersionUID = 1L;

    private String ak;

    private String sk;

    private String region;

    private String project_id;

    private String pipeline_id;

    private String topicUrn;

    public String getAk() {
        return ak;
    }

    public void setAk(String ak) {
        this.ak = ak;
    }

    public String getSk() {
        return sk;
    }

    public void setSk(String sk) {
        this.sk = sk;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getProject_id() {
        return project_id;
    }

    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getPipeline_id() {
        return pipeline_id;
    }

    public void setPipeline_id(String pipeline_id) {
        this.pipeline_id = pipeline_id;
    }

    public String getTopicUrn() {
        return topicUrn;
    }

    public void setTopicUrn(String topicUrn) {
        this.topicUrn = topicUrn;
    }
}

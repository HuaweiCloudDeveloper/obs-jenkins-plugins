package com.huawei.jenkins.vss;

import org.apache.commons.lang.StringUtils;
import org.kohsuke.stapler.DataBoundSetter;

import java.io.Serializable;

/**
 * 用户自定义输出参数
 */
public class CustomInput implements Serializable {
    private static final long serialVersionUID = 1L;

    private String ak;

    private String sk;

    private String region;

    //是否将本次扫描升级为专业版规格(¥99.00/次)
    private String upgrade;

    //扫描任务的名称
    private String task_name;

    //被扫描的目标网址
    private String url;

    //扫描任务类型
    private String task_type;

    //普通任务的定时启动时间
    private String timer;

    //监测任务的定时触发时间
    private String trigger_time;

    //监测任务的定时触发周期
    private String task_period;

    //扫描模式:fast - 快速扫描 normal - 标准扫描 deep - 深度扫描
    private String scan_mode;

    //是否进行端口扫描
    private String port_scan;

    //是否进行弱密码扫描
    private String weak_pwd_scan;

    //是否进行CVE漏洞扫描
    private String cve_check;

    //是否进行网站内容合规文字检测
    private String text_check;

    //是否进行网站内容合规图片检测
    private String picture_check;

    //是否进行网站挂马检测
    private String malicious_code;

    //是否进行链接健康检测(死链、暗链、恶意外链)
    private String malicious_link;

    private String topicUrn;

    public String getAk() {
        return ak;
    }

    public void setAk(String ak) {
        this.ak = ak;
    }

    public String getSk() {
        return sk;
    }

    public void setSk(String sk) {
        this.sk = sk;
    }

    public String getRegion() {
        return region;
    }
    public void setRegion(String region) {
        this.region = region;
    }

    public String getUpgrade() {
        return upgrade;
    }

    public void setUpgrade(String upgrade) {
        this.upgrade = upgrade;
    }

    public String getTask_name() {
        return task_name;
    }

    public void setTask_name(String task_name) {
        this.task_name = task_name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTask_type() {
        return task_type;
    }

    public void setTask_type(String task_type) {
        this.task_type = task_type;
    }

    public String getTimer() {
        return timer;
    }

    public void setTimer(String timer) {
        this.timer = timer;
    }

    public String getTrigger_time() {
        return trigger_time;
    }

    public void setTrigger_time(String trigger_time) {
        this.trigger_time = trigger_time;
    }

    public String getTask_period() {
        return task_period;
    }

    public void setTask_period(String task_period) {
        this.task_period = task_period;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getScan_mode() {
        return scan_mode;
    }

    public void setScan_mode(String scan_mode) {
        this.scan_mode = scan_mode;
    }

    public String getPort_scan() {
        return port_scan;
    }

    public void setPort_scan(String port_scan) {
        this.port_scan = port_scan;
    }

    public String getWeak_pwd_scan() {
        return weak_pwd_scan;
    }

    public void setWeak_pwd_scan(String weak_pwd_scan) {
        this.weak_pwd_scan = weak_pwd_scan;
    }

    public String getCve_check() {
        return cve_check;
    }

    public void setCve_check(String cve_check) {
        this.cve_check = cve_check;
    }

    public String getText_check() {
        return text_check;
    }

    public void setText_check(String text_check) {
        this.text_check = text_check;
    }

    public String getPicture_check() {
        return picture_check;
    }

    public void setPicture_check(String picture_check) {
        this.picture_check = picture_check;
    }

    public String getMalicious_code() {
        return malicious_code;
    }

    public void setMalicious_code(String malicious_code) {
        this.malicious_code = malicious_code;
    }

    public String getMalicious_link() {
        return malicious_link;
    }

    public void setMalicious_link(String malicious_link) {
        this.malicious_link = malicious_link;
    }

    public String getTopicUrn() {
        return topicUrn;
    }

    public void setTopicUrn(String topicUrn) {
        this.topicUrn = topicUrn;
    }
}

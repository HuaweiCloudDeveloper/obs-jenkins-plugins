/*
 * Copyright 2022. Huawei Technologies Co., Ltd. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.huawei.jenkins.codeCheck;

import hudson.EnvVars;
import hudson.Extension;
import hudson.FilePath;
import hudson.model.TaskListener;
import org.jenkinsci.plugins.workflow.steps.*;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.DataBoundSetter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * pipeline语法支持obs文件上传
 */
public class codeCheckPipelineStep extends Step {

    private String ak;

    private String sk;

    private String region;

    private String task_id;

    private String project_id;

    private String language;

    private String gitBranch;

    private String gitUrl;

    private String passWord;

    private String userName;

    private String deleteTask;

    private String topicUrn;

    public String getAk() {
        return ak;
    }

    @DataBoundSetter
    public void setAk(String ak) {
        this.ak = ak;
    }

    public String getSk() {
        return sk;
    }

    @DataBoundSetter
    public void setSk(String sk) {
        this.sk = sk;
    }

    public String getRegion() {
        return region;
    }

    @DataBoundSetter
    public void setRegion(String region) {
        this.region = region;
    }

    public String getTask_id() {
        return task_id;
    }

    @DataBoundSetter
    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getProject_id() {
        return project_id;
    }

    @DataBoundSetter
    public void setProject_id(String project_id) {
        this.project_id = project_id;
    }

    public String getLanguage() {
        return language;
    }

    @DataBoundSetter
    public void setLanguage(String language) {
        this.language = language;
    }

    public String getGitBranch() {
        return gitBranch;
    }

    @DataBoundSetter
    public void setGitBranch(String gitBranch) {
        this.gitBranch = gitBranch;
    }

    public String getGitUrl() {
        return gitUrl;
    }

    @DataBoundSetter
    public void setGitUrl(String gitUrl) {
        this.gitUrl = gitUrl;
    }

    public String getPassWord() {
        return passWord;
    }

    @DataBoundSetter
    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getUserName() {
        return userName;
    }

    @DataBoundSetter
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getDeleteTask() {
        return deleteTask;
    }

    @DataBoundSetter
    public void setDeleteTask(String deleteTask) {
        this.deleteTask = deleteTask;
    }

    public String getTopicUrn() {
        return topicUrn;
    }

    @DataBoundSetter
    public void setTopicUrn(String topicUrn) {
        this.topicUrn = topicUrn;
    }

    @DataBoundConstructor
    public codeCheckPipelineStep(String region) {
        this.region = region;
    }

    @Override
    public StepExecution start(StepContext context) throws Exception {
        return new codeCheckPipelineStep.Execution(this, context);
    }

    @Extension
    public static class DescriptorImpl extends StepDescriptor {

        @Override
        public Set<? extends Class<?>> getRequiredContext() {
            return requires(TaskListener.class, EnvVars.class, FilePath.class);
        }

        @Override
        public String getFunctionName() {
            return "CodeCheckPipeline";
        }

        @Override
        public String getDisplayName() {
            return "华为云CodeArts check代码检查";
        }
    }

    public static <T extends Class<?>> Set<T> requires(T... classes) {
        return new HashSet<>(Arrays.asList(classes));
    }

    public static class Execution extends SynchronousNonBlockingStepExecution<String> {

        protected static final long serialVersionUID = 1L;

        protected final transient codeCheckPipelineStep step;

        public Execution(codeCheckPipelineStep step, StepContext context) {
            super(context);
            this.step = step;

        }

        @Override
        public String run() throws Exception {
            CustomInput customInput = new CustomInput();
            customInput.setAk(step.getAk());
            customInput.setSk(step.getSk());
            customInput.setRegion(step.getRegion());
            customInput.setProject_id(step.getProject_id());
            customInput.setTask_id(step.getTask_id());
            customInput.setGitBranch(step.getGitBranch());
            customInput.setLanguage(step.getLanguage());
            customInput.setGitUrl(step.getGitUrl());
            customInput.setPassWord(step.getPassWord());
            customInput.setUserName(step.getUserName());
            customInput.setDeleteTask(step.getDeleteTask());
            customInput.setTopicUrn(step.getTopicUrn());
            TaskListener listener = codeCheckPipelineStep.Execution.this.getContext().get(TaskListener.class);
            return codeCheckService.runTask(listener, customInput);
        }
    }
}
